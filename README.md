# beyondtracks-photon

[Photon](https://github.com/komoot/photon) is an open source geocoder for OpenStreetMap data.

This repository will buld a fresh search index for Photon and deploy that to our hosted geocoding service running Photon.

## Benchmarking

- On a DigitalOcean `c-4` CPU optimised server (8GB RAM, 4CPU, 50GB SSD), processing Australia took 1h 43min.
- Testing with 4GB RAM ran into out of memory issues.
- Most of the processing takes advantage of multicores.
- The resulting `photon_data` contents were about 1.2GB.
- Running the photon search server requires  0.4-1GB of RAM.
